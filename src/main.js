import Vue from 'vue'
import App from './App.vue'
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';

// import CxyUI from './package/index.js'
// Vue.use(CxyUI);
// import {cxyButton, cxySelect} from './package/index.js'
// Vue.component(cxyButton.name, cxyButton)
// Vue.component(cxySelect.name, cxySelect)

import cxyElement from 'cxy-element'
import 'cxy-element/style/cxy-element.css'
Vue.use(cxyElement);

Vue.use(ElementUI);
Vue.config.productionTip = false

new Vue({
  render: h => h(App),
}).$mount('#app')
