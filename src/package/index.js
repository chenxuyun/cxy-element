 // 引入封装好的组件
import cxyAnimation from './cxy-animation/index.vue';
import cxyButton from './cxy-button/index.vue';
import cxyPopup from './cxy-popup/index.vue';
import cxySelect from './cxy-select/index.vue';
import cxyPagination from './cxy-pagination/index.vue';
import cxyLayout from './cxy-layout/index.vue';

const coms = [
    cxyAnimation,
    cxyButton,
    cxyPopup,
    cxySelect,
    cxyPagination,
    cxyLayout
];

// 批量注册组件
const install = function (Vue) {
    coms.forEach((com) => {
        Vue.component(com.name, com)
    })
};

export default install;
export {
    cxyAnimation,
    cxyButton,
    cxyPopup,
    cxySelect,
    cxyPagination,
    cxyLayout
};