**cxy-element**

基于vue2.x对element-ui 2.15.4的组件进行二次封装


作者：chenxuyun



**一、安装**



```javascript
npm i cxy-element
```



或

```javascript
yarn add cxy-element
```



**二、引入**



在vue项目的入口文件 main.js 中：

```javascript
import cxyElement from 'cxy-element'

import 'cxy-element/style/cxy-element.css'

Vue.use(cxyElement)

// or

import {

cxyButton,

cxySelect

// ...

} from 'cxy-element'

Vue.component(cxyButton.name, cxyButton)

Vue.component(cxySelect.name, cxySelect)

```





**三、基本使用**

```html
<cxy-button text="按钮" />
<cxy-select title="多选" data="options" multiple />
```

